
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trung",
     home: Scaffold(
       appBar: AppBar(
         title: Text('My first App'),
       ),
       body: Center(
         child: Text('Hello My Friends!!'),
       ),
     ),
    );
  }
}

// class MyFirstApp extends StatelessWidget {
//   const MyFirstApp({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home:
//     );
//   }
// }

