class Person{
  Person({
    required this.name,
    required this.age,
    required this.height,
  });

  final String name;
  final int age;
  final double height;

  void printDescription(){
    print("My name is$name.I'm$age years old,I'm$height meters tall.");
    print('1');
  }
}

void main(){

  final p1=Person(name:'Andrea',age:36,height:1.84);
  p1.printDescription();
}